package testing;
import static org.junit.Assert.*;
import java.util.ArrayList;
import org.junit.*;
import PT2019.demo.DemoProject.*;

public class OperationsTest{
	Polinomial p1;
	Polinomial p2;
	
	@Before
	public void setUp(){
		 p1 = new Polinomial(new Monomial(0,1));
		 p2 = new Polinomial(new Monomial(0,1));
	} 
	
	@Test
	public void testAditionBothPoliZero(){
		p2.addTerm(0,-1);
		p1.addTerm(0,-1);
		Polinomial result = Operations.add(p1, p2);
		assertNotNull(result.toString());
		assertEquals(result.toString(), "0");
	}
	
	@Test
	public void testAditionOnePoliZero(){
		p2.addTerm(0,-1);
		Polinomial result = Operations.add(p1, p2);
		assertNotNull(result.toString());
		assertEquals(result.toString(), "1");
	}
	
	@Test
	public void testAditionSameDegreePoli(){
		Polinomial result = Operations.add(p1, p2);
		assertNotNull(result.toString());
		assertEquals(result.toString(), "2");
	}
	
	@Test
	public void testAddDifferentNonZeroDegree(){
		p1.addTerm(5, 4);
		Polinomial result = Operations.add(p1, p2);
		assertNotNull(result.toString());
		assertEquals(result.toString(), "4x^5+2");
	}
	
	@Test
	public void testSubtractBothPoliZero(){
		p2.addTerm(0,-1);
		p1.addTerm(0,-1);
		Polinomial result = Operations.subtract(p1, p2);
		assertNotNull(result.toString());
		assertEquals(result.toString(), "0");
	}
	
	@Test
	public void testSubtractOnePoliZero(){
		p2.addTerm(0,-1);
		Polinomial result = Operations.subtract(p1, p2);
		assertNotNull(result.toString());
		assertEquals(result.toString(), "1");
	}
	
	@Test
	public void testSubtractSameDegreePoli(){
		Polinomial result = Operations.subtract(p1, p2);
		assertNotNull(result.toString());
		assertEquals(result.toString(), "0");
	}
	
	@Test
	public void testSubtractDifferentNonZeroDegree(){
		p1.addTerm(5, 4);
		Polinomial result = Operations.subtract(p1, p2);
		assertNotNull(result.toString());
		assertEquals(result.toString(), "4x^5");
	}
	
	@Test
	public void testMultiplyBy0(){
		p2.addTerm(0, -1);
		Polinomial result = Operations.multiply(p1, p2);
		assertNotNull(result.toString());
		assertEquals(result.toString(), "0");
	}
	
	@Test
	public void testMultiplyBy1(){
		p1.addTerm(5, 4);
		Polinomial result = Operations.multiply(p1, p2);
		assertNotNull(result.toString());
		assertEquals(result.toString(), "4x^5+1");
	}
	
	@Test
	public void testMultiplySameDegree(){
		p1.addTerm(2, 4);
		p2.addTerm(2, 8);
		Polinomial result = Operations.multiply(p1, p2);
		assertNotNull(result.toString());
		assertEquals(result.toString(), "32x^4+12x^2+1");
	}
	
	@Test
	public void testMultiplyDifDegree(){
		p1.addTerm(1, 4);
		p2.addTerm(3, 3);
		Polinomial result = Operations.multiply(p1, p2);
		assertNotNull(result.toString());
		assertEquals(result.toString(), "12x^4+3x^3+4x+1");
	}
	
	@Test
	public void testDerivateConstant(){
		Polinomial result = Operations.derivate(p1);
		assertNotNull(result.toString());
		assertEquals(result.toString(), "0");
	}
	
	@Test
	public void testDerivateOneTermDegreeOne(){
		p1.addTerm(0, -1);
		p1.addTerm(1, 4);
		Polinomial result = Operations.derivate(p1);
		assertNotNull(result.toString());
		assertEquals(result.toString(), "4");
	}
	
	@Test
	public void testDerivateOneTermRandomDegree(){
		p1.addTerm(0, -1);
		p1.addTerm(4, 4);
		Polinomial result = Operations.derivate(p1);
		assertNotNull(result.toString());
		assertEquals(result.toString(), "16x^3");
	}
	
	@Test
	public void testDerivateMultipleTerms(){
		p1.addTerm(1, 4);
		p1.addTerm(3, 3);
		p1.addTerm(2, 4);
		Polinomial result = Operations.derivate(p1);
		assertNotNull(result.toString());
		assertEquals(result.toString(), "9x^2+8x+4");
	}
	
	@Test
	public void testIntegrateNonZeroConstant(){
		Polinomial result = Operations.integrate(p1);
		assertNotNull(result.toString());
		assertEquals(result.toString(), "x");
	}
	
	@Test
	public void testIntegrateZero(){
		p1.addTerm(0,-1);
		Polinomial result = Operations.integrate(p1);
		assertNotNull(result.toString());
		assertEquals(result.toString(), "0");
	}
	
	@Test
	public void testIntegrateMonom(){
		p1.addTerm(0,-1);
		p1.addTerm(3,4);
		Polinomial result = Operations.integrate(p1);
		assertNotNull(result.toString());
		assertEquals(result.toString(), "x^4");
	}
	
	@Test
	public void testIntegrateMultipleTerms(){
		p1.addTerm(0,-1);
		p1.addTerm(3,4);
		p1.addTerm(4,4);
		Polinomial result = Operations.integrate(p1);
		assertNotNull(result.toString());
		assertEquals(result.toString(), "0.8x^5+x^4");
	}
	
	@Test
	public void testDivisionByZero(){
		p2.addTerm(0,-1);
		DivisionResult result = Operations.divide(p1,p2);
		assertNull(result);
	}
	
	@Test
	public void testDivisionByHigherDegreePoli(){
		p2.addTerm(2,1);
		DivisionResult result = Operations.divide(p1,p2);
		assertNull(result);
	}
	
	@Test
	public void testDivisionBy1(){
		p1.addTerm(2,1);
		DivisionResult result = Operations.divide(p1,p2);
		assertNotNull(result.getQuotien().toString());
		assertNotNull(result.getReminder().toString());
		assertEquals(result.getQuotien().toString(), "x^2+1");
		assertEquals(result.getReminder().toString(), "0");
	}
	
	@Test
	public void testDivisionByRandomPoli(){
		p1.addTerm(4,4);
		p1.addTerm(3,4);
		p1.addTerm(0,-1);
		p2.addTerm(2,4);
		DivisionResult result = Operations.divide(p1,p2);
		assertNotNull(result.getQuotien().toString());
		assertNotNull(result.getReminder().toString());
		assertEquals(result.getQuotien().toString(), "x^2+x-0.25");
		assertEquals(result.getReminder().toString(), "-1x+0.25");
	}
	
}
