package testing;
import static org.junit.Assert.*; 
import org.junit.*;
import PT2019.demo.DemoProject.*;

public class PolinomialTest{
	Polinomial p1;
	
	@Before
	public void setUp(){
		 p1 = new Polinomial(new Monomial(0,1));
	} 
	
	@Test(expected = IllegalArgumentException.class)
	public void testAddNegativeDegreeTerm(){
		p1.addTerm(-1, 0);
	}
	
	@Test
	public void testAddZeroTerm(){
		p1.addTerm(0, 0);
		assertNotNull(p1.toString());
		assertEquals(p1.toString(), "1");
	}
	
	@Test
	public void testAddSameDegree(){
		p1.addTerm(0, 1);
		p1.addTerm(0, 1);
		assertNotNull(p1.toString());
		assertEquals(p1.toString(), "3");
	}
	
	@Test
	public void testAddRandomTerms(){
		p1.addTerm(5, 2);
		p1.addTerm(3, 2);
		assertNotNull(p1.toString());
		assertEquals(p1.toString(), "2x^5+2x^3+1");
	}
	
	@Test
	public void testGetMaxDegreeNotEmpty(){
		p1.addTerm(5, 2);
		assertNotNull(p1.toString());
		assertEquals(p1.maxDegree(),5);
	}
	
	@Test
	public void testGetMaxDegreeEmpty(){
		p1.addTerm(0, -1);
		assertNotNull(p1.toString());
		assertEquals(p1.maxDegree(),-1);
	}
	
	
	@Test
	public void testGetCoeffPositiveExistentDegree() {
		assertEquals(1, (int)p1.getCoeff(0));
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testGetCoeffNegative() {
		p1.getCoeff(-1);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testGetCoeffPositiveNonExistentDegree() {
		p1.getCoeff(5);
	}
	
}
