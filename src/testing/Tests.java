package testing;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
 PolinomialTest.class,
 OperationsTest.class
}) 

public class Tests {

}
