package GUI;
import java.awt.*;
import java.awt.event.ActionListener;

import javax.swing.*;

public class View extends JFrame{
	private JPanel resultPanel;
	private JPanel inputPanel;
	private JTextField polynomial1Tf;
	private JTextField polynomial2Tf;
	private JTextField resultTf;
	private JButton multiplyBt;
	private JButton addBt;
	private JButton subtractBt;
	private JButton divideBt;
	private JButton integrateBt;
	private JButton derivateBt;
	public View(){
		init();
		reset();
		resultPanel.add(new JLabel("Polynomial1:"));
		resultPanel.add(polynomial1Tf);
		resultPanel.add(new JLabel("Polynomial2:"));
		resultPanel.add(polynomial2Tf);
		resultPanel.add(new JLabel("Result:"));
		resultPanel.add(resultTf);
		inputPanel.add(addBt);
		inputPanel.add(subtractBt);
		inputPanel.add(multiplyBt);
		inputPanel.add(divideBt);
		inputPanel.add(integrateBt);
		inputPanel.add(derivateBt);
		
		this.setResizable(false);
		this.setLocationRelativeTo(null);
		this.setLayout(new GridLayout(2, 1));
		this.add(resultPanel);
		this.add(inputPanel);
		this.setSize(500,200);
		this.setTitle("Polynomials");
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	public void reset() {
		resultTf.setText("0");
		polynomial1Tf.setText("");
		polynomial2Tf.setText("");
	}
	
	private void init() {
		resultPanel = new JPanel();
		inputPanel = new JPanel();
		polynomial1Tf = new JTextField(300);
		polynomial2Tf = new JTextField(300);
		resultTf = new JTextField(300);
		multiplyBt = new JButton("Multiply");
		divideBt = new JButton("Divide");
		addBt = new JButton("Add");
		subtractBt = new JButton("Subtract");
		integrateBt = new JButton("Integrate");
		derivateBt = new JButton("Derivate");
	
		resultTf.setEditable(false);
		resultPanel.setLayout(new GridLayout(3,2));
		inputPanel.setLayout(new GridLayout(3,2));
	}
	
	public void setResult(String value) {
		resultTf.setText(value);
	}
	
	public String getPoli(int chooser) {
		if(chooser == 1)
			return polynomial1Tf.getText();
		return polynomial2Tf.getText();
	}
	
	void addMultiplyBtnListener(ActionListener ac) {
		multiplyBt.addActionListener(ac);
	}
	
	void addDivideBtnListener(ActionListener ac) {
		divideBt.addActionListener(ac);
	}
	
	void addAddBtnListener(ActionListener ac) {
		addBt.addActionListener(ac);
	}
	
	void addSubtractBtnListener(ActionListener ac) {
		subtractBt.addActionListener(ac);
	}
	
	void addIntegrateBtnListener(ActionListener ac) {
		integrateBt.addActionListener(ac);
	}
	
	void addDerivateBtnListener(ActionListener ac) {
		derivateBt.addActionListener(ac);
	}

	public void invalidInput(String error) {
		JOptionPane.showMessageDialog(this, error,"Input error", JOptionPane.ERROR_MESSAGE);
	}
	
}
