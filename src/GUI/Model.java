package GUI;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.ArrayList;
import java.util.Iterator;

import PT2019.demo.DemoProject.*;

public class Model {
	Polinomial poli1;
	Polinomial poli2;
	
	public Model() {
		resetPoli();
	}
	
	private boolean processString(String coeffStr, String degreeStr, String fullStr, ArrayList<Float> coeffArray, ArrayList<Integer> degreeArray) {
		Integer degree = 0;
		Float coeff = -1f;
		  	if(coeffStr == null && degreeStr == null && fullStr.contains("x")) {
				  degree = 1;
				  coeff = (float)1;
		   }else {
			   if(coeffStr == null)
				   coeff = fullStr.contains("x") ? (float)1 : (float)0;
			   else coeff=Float.parseFloat(coeffStr);
			   if(degreeStr == null || degreeStr.equals(""))
				    degree= fullStr.contains("x") ? 1 : 0;
			   else degree=Integer.parseInt(degreeStr);
			   if(degree<0)
				   return false;
		   }
		   coeffArray.add(coeff);
		   degreeArray.add(degree);
		   return true;
	}
	
	private boolean validateString(String exp) {
		final String regex = "([Xa-wA-WXy-zY-Z\\[\\\\\\$\\|\\?\\*\\(\\)\\[\\]\\{\\}~@#!%&='\\\":;><\\/,]+)|(\\^[x])|(\\d\\^)|(\\^[+-])";
		Pattern pattern = Pattern.compile(regex, Pattern.MULTILINE);
		Matcher matcher = pattern.matcher(exp);
		return matcher.find();
	} 
	
	public boolean setPoli(String exp, int chooser) {
		if(validateString(exp)) return false;
		Pattern pattern = Pattern.compile("(-?[0-9]*\\.[0-9]+|-?\\b\\d+)?[x]?\\^?(-?\\d+\\b|)?", Pattern.MULTILINE);
		Matcher matcher = pattern.matcher(exp);
		ArrayList<Float> coeffArray = new ArrayList<>();
		ArrayList<Integer> degreeArray= new ArrayList<>();
		boolean ok = false;
		while (matcher.find()) {
		   ok = processString(matcher.group(1), matcher.group(2), matcher.group(0), coeffArray, degreeArray);
		}
		Iterator<Float> a = coeffArray.iterator();
		Iterator<Integer> b = degreeArray.iterator();
		while (a.hasNext() && b.hasNext()) {
			if(chooser==1)
				poli1.addTerm(b.next().intValue(), a.next().floatValue());
			else poli2.addTerm(b.next().intValue(), a.next().floatValue());
		}
		return ok;
	}

	public Polinomial getPoli(int chooser) {
		if(chooser==1)
			return poli1;
		else return poli2;
	}
	
	public void resetPoli() {
		poli1 = new Polinomial();
		poli2 = new Polinomial();
	}
	
	public String addResult() {
		return Operations.add(poli1,poli2).toString();
	}
	
	public String subtractResult() {
		return Operations.subtract(poli1,poli2).toString();
	}
	
	public String multiplyResult() {
		return Operations.multiply(poli1,poli2).toString();
	}
	
	public String divisionResult() {
		DivisionResult result= Operations.divide(poli1,poli2);
		if(result!=null)
			return result.getQuotien().toString() + "+" + result.getReminder();
		return null;
	}
	
	public String derivateResult() {
		return Operations.derivate(poli1).toString();
	}
	
	public String integrateResult() {
		return Operations.integrate(poli1).toString();
	}

}

