package GUI;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


public class Controller {
	Model model;
	View view;
	public Controller(){
		view = new View();
		model = new Model();
		view.addAddBtnListener(new AddListener());
		view.addSubtractBtnListener(new SubtractListener());
		view.addMultiplyBtnListener(new MultiplyListener());
		view.addDivideBtnListener(new DivideListener());
		view.addDerivateBtnListener(new DerivateListener());
		view.addIntegrateBtnListener(new IntegrateListener());
		view.setVisible(true);
	}
	
	private boolean setPolis(boolean onePolinomial) {
		if(!model.setPoli(view.getPoli(1),1)) {
			view.invalidInput("Please inser a valid polinomial1");
			return false;
		}else {
			if(!model.setPoli(view.getPoli(2),2) && !onePolinomial) {
				view.invalidInput("Please inser a valid polinomial2");
				return false;
			}
		}
		return true;
	}
	
	class AddListener implements ActionListener{
		public void actionPerformed(ActionEvent ev) {
			if(setPolis(false))
				view.setResult(model.addResult());
			model.resetPoli();
		}
	}
	
	class SubtractListener implements ActionListener{
		public void actionPerformed(ActionEvent ev) {
			if(setPolis(false))
				view.setResult(model.subtractResult());
			model.resetPoli();
		}
	}
	
	class MultiplyListener implements ActionListener{
		public void actionPerformed(ActionEvent ev) {
			if(setPolis(false))
				view.setResult(model.multiplyResult());
			model.resetPoli();
		}
	}
	
	class DivideListener implements ActionListener{
		public void actionPerformed(ActionEvent ev) {
			if(setPolis(false)){
				String result = model.divisionResult();
				if(result == null)
					view.invalidInput("Cannot divide! Polynomial2 cannot be 0 or have a higher degree than Polynomial1");
				else view.setResult(model.divisionResult());
			}
			model.resetPoli();
		}
	}
	
	class IntegrateListener implements ActionListener{
		public void actionPerformed(ActionEvent ev) {
			if(setPolis(true))
				view.setResult(model.integrateResult());
			model.resetPoli();
		}
	}
	
	class DerivateListener implements ActionListener{
		public void actionPerformed(ActionEvent ev) {
			if(setPolis(true))
				view.setResult(model.derivateResult());
			model.resetPoli();
		}
	}
}
