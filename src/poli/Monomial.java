package poli;
public class Monomial{
	private float coeff;
	private int degree;

	public Monomial(int fdegree, float fcoeff){
		if(fdegree<0)
			throw new IllegalArgumentException("Invalid exponent. Exponent cannot be negative!: " + fdegree);
		degree = fdegree;
		coeff = fcoeff;
	}

	public void setCoeff(float fcoeff) {
		coeff = fcoeff;
	}

	public float getCoeff() {
		return coeff;
	}

	public int getDegree() {
		return degree;
	}

	public String toString() {
		int integer = Math.round(coeff);
		if(coeff==0)
			return "";
		if(degree==0)
			if(integer==coeff)
				return integer + "";
			else return coeff + "";
		if(degree==1 && coeff==1)
			return "x";
		if(coeff==1)
			return "x^" + degree;
		if(degree==1)
			if(integer==coeff)
				return integer + "x";
			else return coeff + "x";
		if(integer == coeff)
			return integer + "x^" + degree;
		else return coeff + "x^" + degree;
	}

	public int compareTo(Monomial term) {
		if(this.getDegree() < term.getDegree())
			return -1;
	    return this.getDegree() == term.getDegree() ? 0 : 1;
	}
}
