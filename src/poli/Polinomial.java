package poli;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
public class Polinomial {
	private ArrayList<Monomial> monoms;
	private final String invalidArgument = "Invalid exponent. Exponent cannot be negative!: ";
	public Polinomial(){
		monoms = new ArrayList<>();
	}

	public Polinomial(Monomial m){
		monoms = new ArrayList<>();
		monoms.add(m);
	}

	public List<Monomial> getMonomList() {
		return new ArrayList<>(monoms);
	}

	public float getCoeff(int degree) {
		if(degree<0)
			throw new IllegalArgumentException(invalidArgument + degree);
		for(Monomial terms : monoms) {
			if(terms.getDegree() == degree) {
				return terms.getCoeff();
			}
		}
		throw new IllegalArgumentException("The term you're trying to set doesn't exist!Degree: " + degree);
	}

	public int maxDegree() {

		if(monoms.isEmpty())
			return -1;
		return monoms.get(0).getDegree();

	}

	public void addTerm(int degree, float coeff) {
		if(degree<0)
			throw new IllegalArgumentException(invalidArgument + degree);
		boolean found = false;
		for(Monomial terms : monoms) {
			if(terms.getDegree() == degree) {
				found = true;
				terms.setCoeff(terms.getCoeff() + coeff);
			}
		}
		if(!found) {
			monoms.add(new Monomial(degree, coeff));
		}
		removeZeroTerms();
		Collections.sort(monoms, (Monomial term2, Monomial term1) -> term1.compareTo(term2));
	}

	public void removeZeroTerms(){
		Iterator<Monomial> itr = monoms.iterator();
		while (itr.hasNext()) {
			if(itr.next().getCoeff() == 0)
				itr.remove();
		}
	}

	public String toString() {
		String result = "";
		int size = monoms.size();
		StringBuilder bld = new StringBuilder();
		if(size>1) {
			for(Monomial terms : monoms) {
				if(monoms.indexOf(terms) != 0 )
					if(terms.getCoeff()>0)
						bld.append("+" + terms.toString());
					else bld.append(terms.toString());
				else bld.append(terms.toString());
			}
			result = bld.toString();
		} else {
				if(size == 1)
					result = monoms.get(0).toString();
				if(result.equals(""))
					result = "0";
		}
		return result;
	}

}
