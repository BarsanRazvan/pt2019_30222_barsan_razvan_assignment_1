package poli;
import java.util.ArrayList;

public class Operations {
	private Operations() {
	}

	public static Polinomial copyPolinomial(Polinomial q){
		Polinomial result = new Polinomial();
		ArrayList<Monomial> qList = (ArrayList<Monomial>)q.getMonomList();
		for(Monomial terms : qList) {
			result.addTerm(terms.getDegree(), terms.getCoeff());
		}
		return result;
	}

	public static Polinomial add(Polinomial p, Polinomial q) {
		ArrayList<Monomial> pList = (ArrayList<Monomial>)p.getMonomList();
		Polinomial result = Operations.copyPolinomial(q);
		for(Monomial pTerms : pList) {
				result.addTerm(pTerms.getDegree(), pTerms.getCoeff());
		}
		return result;
	}

	public static Polinomial subtract(Polinomial p, Polinomial q) {
		ArrayList<Monomial> qList = (ArrayList<Monomial>)q.getMonomList();
		Polinomial result = Operations.copyPolinomial(p);;
		for(Monomial qTerms : qList) {
			result.addTerm(qTerms.getDegree(), qTerms.getCoeff() * -1);
		}
		return result;
	}

	public static Polinomial multiply(Polinomial p, Polinomial q) {
		Polinomial result = new Polinomial();
		ArrayList<Monomial> pList = (ArrayList<Monomial>)p.getMonomList();
		ArrayList<Monomial> qList = (ArrayList<Monomial>)q.getMonomList();
		for(Monomial pTerms : pList) {
			int pDegree = pTerms.getDegree();
			float pCoeff = pTerms.getCoeff();
			for(Monomial qTerms : qList) {
				result.addTerm(pDegree + qTerms.getDegree(), pCoeff * qTerms.getCoeff());
			}
		}
		return result;
	}

	public static Polinomial derivate(Polinomial p){
		ArrayList<Monomial> pList = (ArrayList<Monomial>)p.getMonomList();
		Polinomial result = new Polinomial();
		int pDegree;
		int newDegree;
		float newCoeff;
		for(Monomial pTerms : pList) {
			pDegree = pTerms.getDegree();
			if(pDegree == 0) {
				newDegree = 0;
				newCoeff = 0;
			} else {
				newCoeff = pTerms.getCoeff() * pDegree;
				newDegree = pDegree - 1;
			}
			result.addTerm(newDegree, newCoeff);
		}
		return result;
	}

	public static DivisionResult divide(Polinomial a, Polinomial b) {
		if(a.maxDegree()<b.maxDegree())
			return null;
		if(b.maxDegree()==-1)
			return null;
		DivisionResult result = new DivisionResult();
		Polinomial qo = new Polinomial();
		Polinomial re = Operations.copyPolinomial(a);
		int bDegree = b.maxDegree();
		int restDegree = re.maxDegree();
		float bCoeff = b.getCoeff(bDegree);
		Monomial term;
		ArrayList<Monomial> rList = (ArrayList<Monomial>)re.getMonomList();
		while(!rList.isEmpty() && restDegree>=bDegree) {
			term = new Monomial(restDegree-bDegree, re.getCoeff(restDegree)/bCoeff);
			qo.addTerm(term.getDegree(), term.getCoeff());
			re = Operations.subtract(re, Operations.multiply(new Polinomial(term), b));
			rList = (ArrayList<Monomial>)re.getMonomList();
			restDegree = re.maxDegree();
		}
		result.setQuotient(qo);
		result.setRemainder(re);
		return result;
	}

	public static Polinomial integrate(Polinomial p) {
		ArrayList<Monomial> pList = (ArrayList<Monomial>)p.getMonomList();
		Polinomial result = new Polinomial();
		int pDegree;
		for(Monomial pTerms : pList) {
			pDegree = pTerms.getDegree() + 1;
			result.addTerm(pDegree, pTerms.getCoeff() / pDegree);
		}
		return result;
	}
}
