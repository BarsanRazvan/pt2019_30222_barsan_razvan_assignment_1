package poli;
public class DivisionResult {
	private Polinomial quotient;
	private Polinomial reminder;

	public DivisionResult() {
		quotient = new Polinomial();
		reminder = new Polinomial();
	}

	public Polinomial getQuotien() {
		return quotient;
	}

	public Polinomial getReminder() {
		return reminder;
	}

	public void setQuotient(Polinomial q) {
		quotient = q;
	}

	public void setRemainder(Polinomial r) {
		reminder = r;
	}
}
